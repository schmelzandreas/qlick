# Qlick Timetrack app written in angularjs

## tasks
The user can create, start, resume and finish tasks. A task has the properties of name, id, location, descriptoini, working time intervals and overall duration.
A tsk can started, pasued and resumed as many times as wished, but only finished once. The time a task needs to finish is the overall duration. Each individual timespans a task has been worked on can be tracked individually.
 
## Dashboard
The dashboard shows a list of all created tasks. It supports filtering by a query string that is amtched on location , name and description. The list can also be sorted by name, creation date (ascending and descengin), spent time. Additionally it is possible to hide all already finished tasks. It will not only show the user the time spent of an task, but its context when and how ofthen a task has been worked on.

Each task in the dashboad provides the functionality to start it. This will set the task in the working stat which will last until the task either has been finished or paused.
The cards on the dashboard show the name, location description and the time it has been worked on. The duration shows tha last interaval the user worked on it addutional the overall spent time.

A finished task is highlighted by a green border on the right side. The gray bar at the bottom of the card shows the overall duration relative to the other tasks. The longest time will have a 100% wide gray bar. 
Value cahgnes are updated in realtime.
 
## New Task
A user can create a new task with name, description and location. Each of the properties is a string and required. The input is validated to fullfil those requirements. An invalid state will highlight with a red bprder and a message showing him which field need to be filled out.
On task createion a notification (if enabled) is shown,
 
## TimeLine
The timeline gives a linear timely overview of when which task has been worked on. As only one task can be worked on at a time this view is distinct. Breaks/ where no specific task has been done is filled with white blanks. The length of the bars represent the linear realtion to the duration spent for this working interval.
As break might fill up a lot of time in the overview its length is capped to be at most 100% with of the entire pagewidth.
 
## Chart
The chart shows the overall worked time per task, Each bar shows one task. The graph is created with charts.js.
 
 
## Additional features
Tasks are abstracted in the taskService, which tracks tasks and time spent indepentendt of a view. Therefore multple controllers/ views can rely on this service as a single source of truth. It saves its state in the localstorage.
The notificationServie is responsible for requesting the notification permission and showing based on users preference a notification or not on task creation, work starting and work finishing, 
 
 
## Requirement-considerations

“Explain how you show notifications on Google Chrome”

the App uses the web notifications api. Depending on the browsers capabilities notifications are requested and shown. 
“Explain how you would pre-populate the location field"

The location fild could be prefilled with the location of the last created or last worked on task. This assumes the user tracks actually where he filled out the task. Another approach would be to use the web location api which gives a lat- and longitude. This could be matched with previous known locations to long and latitude or use a web service that mathes geolocation to location names.
 
## backend
No backend has been implemented. The informationis stored on the localstorage in json. This json blob would be required to store on a server and reload from there again. 
We assume that only one task can be worked on at a time. Starting a different task while working on another one will pause the current one. Creating a task will not automatically start the task to be worked on as we assume that a user want to shedule mutiple tasks first and then start to work on them one by one



## qliktimetrack

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

### Build & development

Run `grunt` for building and `grunt serve` for preview.

### Testing

Running `grunt test` will run the unit tests with karma.
