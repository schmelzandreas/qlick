'use strict';


angular.module('qliktimetrackApp')
  .factory('NotifyService', ['$interval', '$rootScope',
   function ($interval, $rootScope) {

    // notifications supported?
    if (!Notification) {
      console.warn('notifications not supported');
      return {
        supported: false,
        active: false,
        notify: function() {},
        setActive: function() {}
      };
    }



    var active = (Notification.permission == "granted");
    console.log('start with ', active);

    function notify(note) {
      if (Notification.permission == "granted") {
        var notification = new Notification(note.title, {
          icon: 'images/qlick.png',
          body: note.body,
        });
      }
    }

    function setActive(bool) {
      if (bool && Notification.permission !== "granted") {
        Notification.requestPermission().then(function(result) {
          if (result === 'denied') {
            console.log('Permission wasn\'t granted. Allow a retry.');
            active = false;
          }
          if (result === 'default') {
            console.log('The permission request was dismissed.');
            active = true;
          }
        })
      }
    }

    setActive(true);

    return {
      supported: true,
      active: active,
      notify: notify,
      setActive: setActive
    };

  }]);
