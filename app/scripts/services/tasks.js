'use strict';


angular.module('qliktimetrackApp')
  .factory('TaskService', ['$interval', '$rootScope', 'NotifyService',
   function ($interval, $rootScope, NotifyService) {

    var tasks = loadTasks();
    var isActive = false;
    var activeTask = null;
    var countInterval = null;

    function saveTasks() {
      localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    function countTime () {
      var time = activeTask.time[activeTask.time.length - 1];
      time.end = getTime();
      activeTask.duration = calcDuration(activeTask);
      saveTasks();
    }

    function calcDuration(task) {
      var duration = 0;
      for (var i = 0; i < task.time.length; i++) {
        duration += task.time[i].end - task.time[i].start;
      }
      return duration / 1000;
    }

    function getTime () {
      return new Date().getTime();
    }

    function loadTasks() {
      var json = localStorage.getItem('tasks');
      if (!json) return [];
      return JSON.parse(json);
    }
    return {
      addTask: function(task) {
        task.id = tasks.length;
        task.time = [];
        task.finished = false;
        task.duration = 0;
        tasks.push(task);
        saveTasks();
      },
      saveTasks: saveTasks,
      getTasksReference: function() {
        return tasks;
      },
      setActive: function(task) {
        isActive = true;
        activeTask = task;
        activeTask.time.push({start: getTime(), end: getTime() });
        $interval.cancel(countInterval);
        countInterval = $interval(countTime, 1000);
        $rootScope.$broadcast('startTask');
      },
      pause: function() {
        $interval.cancel(countInterval);
        countTime();
        isActive = false;
        saveTasks();
        $rootScope.$broadcast('pauseTask');
      },
      finish: function() {
        $interval.cancel(countInterval);
        countTime();
        isActive = false;
        activeTask.finished = true;
        activeTask = null;
        saveTasks();
        NotifyService.notify({
          title: 'Task finished',
          body: 'Task' + activeTask.name + ' has been finished after '+ activeTask.duration
        });
        $rootScope.$broadcast('endTask');
      },
      getActiveTask: function() {
        console.log('return ',activeTask);
        return activeTask;
      },
      isPaused: function() {
        return !isActive;
      }
    }

  }]);
