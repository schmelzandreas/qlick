'use strict';

angular.module('qliktimetrackApp')
  .filter('formatDuration', function() {
    function formatSec(dur) {
      if (dur < 60) {
        return Math.floor(dur) + 's';
      } else if (dur < 60*60) {
        return Math.floor(dur / 60) + 'm ' + (dur % 60) + 's';
      } else {
        return Math.floor(dur / 3600) + 'h ' + Math.floor((dur % 3600)/ 60) + 'm';
      }
    }

    return function(task) {
      if (task.time.length == 0) return '';
      var time = task.time[task.time.length - 1];
      var dur = Math.floor((time.end - time.start) / 1000);  // in seconds
      var totalStr = '';
      if (task.time.length > 1) {
        var total = 0;
        for (var i = 0; i < task.time.length; i++) {
          total += Math.floor((task.time[i].end - task.time[i].start)/1000);
        }
        totalStr += ' (total: ' + formatSec(total) + ')';

      }
      return formatSec(dur) + totalStr;

    };
  });

