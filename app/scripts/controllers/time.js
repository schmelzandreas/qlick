'use strict';

var global;
angular.module('qliktimetrackApp')
  .controller('TimeCtrl', ['$scope', 'TaskService', '$filter',
   function ($scope, TaskService, $filter) {

    var colors = ["#00ffff", "#f5f5dc", "#000000", "#0000ff", "#a52a2a", "#00ffff", "#00008b", "#008b8b", "#a9a9a9", "#006400", "#bdb76b", "#8b008b", "#556b2f", "#ff8c00", "#9932cc", "#8b0000", "#e9967a", "#9400d3", "#ff00ff", "#ffd700", "#008000", "#4b0082", "#f0e68c", "#add8e6", "#e0ffff", "#90ee90", "#d3d3d3", "#ffb6c1", "#ffffe0", "#00ff00", "#ff00ff", "#800000", "#000080", "#808000", "#ffa500", "#ffc0cb", "#800080", "#800080", "#ff0000", "#c0c0c0", "#ffffff", "#ffff00"];

    var tasks = TaskService.getTasksReference();

    var times = [];
    var maxDur = 0;
    for (var i = 0; i < tasks.length; i++) {
      var task = tasks[i];
      for (var j = 0; j < task.time.length; j++) {
        times.push({
          id: i,
          start: task.time[j].start,
          end: task.time[j].end
        });
        maxDur = Math.max(task.time[j].end - task.time[j].start, maxDur);
      }
    }
    console.log('max', maxDur);
    times = _.sortBy(times, 'start');
    // fill gaps = breaks
    var end = times.length
    for (var i = 1; i < end; i++) {
      times.push({
        id: -1,
        start: times[i - 1].end,
        end: times[i].start
      })
    }
    times = _.sortBy(times, 'start');

    $scope.times = times;

    $scope.getWidth = function(time) {
      var perc = (time.end - time.start) / maxDur * 100;
      perc = Math.min(100, perc);
      return perc;
    };

    $scope.getColor = function(id) {
      if (id == -1) {
        return 'transparent';
      } else {
        return colors[id % colors.length];
      }
    }

    $scope.tasks = tasks;

  }]);
