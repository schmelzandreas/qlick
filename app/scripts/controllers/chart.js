'use strict';

angular.module('qliktimetrackApp')
  .controller('ChartCtrl', ['$scope', 'TaskService', '$filter', '$rootScope', 'NotifyService',
   function ($scope, TaskService, $filter, $rootScope, NotifyService) {


    var tasks = TaskService.getTasksReference();
    var durs = [];
    var labels = [];
    for (var i = 0; i < tasks.length; i++) {
      durs.push(tasks[i].duration);
      labels.push(tasks[i].name);
    }
    console.log(durs);

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: labels,
        datasets: [{
            label: 'duration spent',
            data: durs,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});



  }]);
