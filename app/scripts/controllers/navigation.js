'use strict';

var global;
angular.module('qliktimetrackApp')
  .controller('NavigationCtrl', ['$scope', '$location', 'TaskService', '$rootScope',
   function ($scope, $location, TaskService, $rootScope) {
    $scope.location = function() {
      return $location.path();
    };

    $scope.activeTask = null;
    $scope.isPaused = false;
    $rootScope.$on('startTask', function() {
      console.log('task started');
      $scope.activeTask = TaskService.getActiveTask();
      $scope.isPaused = TaskService.isPaused();
    });
    $rootScope.$on('pauseTask', function() {
      console.log('task paused');
      $scope.isPaused = TaskService.isPaused();
    });
    $rootScope.$on('endTask', function() {
      console.log('task ended');
      $scope.activeTask = null;
      $scope.isPaused = TaskService.isPaused();
    });

    $scope.pauseTask = function() {
      TaskService.pause();
    };

    $scope.finishTask = function() {
      TaskService.finish();
    };

    $scope.resumeTask = function() {
      TaskService.setActive($scope.activeTask);
    };

  }]);
