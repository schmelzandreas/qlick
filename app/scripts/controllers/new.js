'use strict';

var global;
angular.module('qliktimetrackApp')
  .controller('NewCtrl', ['$scope', 'TaskService', 'NotifyService',
   function ($scope, TaskService, NotifyService) {
    // Code goes here
    global = TaskService;

    $scope.tasks = TaskService.getTasksReference();


    $scope.createTask = function($event) {
      if ($scope.newTaskForm.location.$valid &&
        $scope.newTaskForm.name.$valid &&
        $scope.newTaskForm.description.$valid) {
        $scope.showError = false;

        var task = {
          name: $scope.name,
          description: $scope.description,
          location: $scope.location
        };

        $scope.name = '';
        $scope.description = '';
        $scope.location = '';
        $scope.newTaskForm.$setUntouched();

        TaskService.addTask(task);
        $event.preventDefault();
        NotifyService.notify({
          title: 'New Task',
          body: 'Task ' + task.name + ' was successfully created'
        });
      } else {
        $scope.showError = true;
      }

      var name = $scope.name;
      var description = $scope.description;
    };


  }]);
