'use strict';

angular.module('qliktimetrackApp')
  .controller('DashboardCtrl', ['$scope', 'TaskService', '$filter', '$rootScope', 'NotifyService',
   function ($scope, TaskService, $filter, $rootScope, NotifyService) {

    $scope.order = 'id';

    $scope.tasks = TaskService.getTasksReference();
    $scope.hideFinished = false;
    $scope.searchInput = '';
    $scope.activeTask = TaskService.getActiveTask();
    $scope.isPaused = TaskService.isPaused();

    $rootScope.$on('startTask', function() {
      $scope.activeTask = TaskService.getActiveTask();
      $scope.isPaused = TaskService.isPaused();
    });
    $rootScope.$on('pauseTask', function() {
      $scope.isPaused = TaskService.isPaused();
    });
    $rootScope.$on('endTask', function() {
      console.log('task ended');
      $scope.activeTask = null;
      $scope.isPaused = TaskService.isPaused();
    });

    $scope.startWork = function(id) {
      var task = _.find($scope.tasks, {id: id});
      if (!task) return;
      TaskService.setActive(task);
    };

    $scope.pause = function() {
      TaskService.pause();
    };

    $scope.resume = function() {
      $scope.startWork($scope.activeTask.id);
    };

    $scope.finish = function() {
      TaskService.finish();
    };

    $scope.getDurationLength = function(task) {
      var max = _.maxBy($scope.tasks, 'duration').duration;
      max = Math.max(1, max); // div by zero
      return (task.duration || 0) / max * 100;
    }

    $scope.taskFilter = function (task) {
      if ($scope.hideFinished && task.finished) {
        return false;
      } else {
        if (!$scope.searchInput) return true;
        var search = $scope.searchInput.toLowerCase();
        return task.name.toLowerCase().indexOf(search) != -1 ||
               task.description.toLowerCase().indexOf(search) != -1 ||
               task.location.toLowerCase().indexOf(search) != -1;
      }
    };

    $scope.notificationsSupported = NotifyService.supported;
    $scope.notificationsActive = NotifyService.active;

    $scope.activateNotifications = function() {
      NotifyService.setActive(true);
    }

  }]);
